## Hier soll ein einfacher Publisher/Subscriber generiert werden

1. Bitte einen catkin Workspace anlegen, falls noch nicht geschehen.
```
    $ mkdir -p ~/catkin_ws/src
    $ cd ~/catkin_ws/
    $ catkin_make
```
2. Anschließend muss ein neues "packages" generiert werden.
Hierzu in das src directory im workspace wechseln
```
    $ cd ~/catkin_ws/src
```
und den Befehl "catkin_create_pkg" benutzen.
catkin_create_pkg braucht einen Namen und die Abhängigkeiten,
also die Namen der zu benutzenden Pakete, die wir für unser
package brauchen. Hier sind wir abhängig von std_msgs, rospy und roscpp
```
    $ catkin_create_pkg pub_sub_tutorial std_msgs rospy roscpp
```
3. Jetzt muss das package gebaut werden. Das geschieht mit catkin_make
```
    $ cd ~/catkin_ws
    $ catkin_make
```
4. ROS benutzt ein umfangreiches namespace System, um die einzelnen Nodes voneinander
zu unterscheiden. Damit das neue package auch erkannt wird, muss es gesourced werden.
```
    $ . ~/catkin_ws/devel/setup.bash
```
5. Bisher ist das package noch ohne Inhalt. Da wir "nur" mit Python programmieren,
sollten wir ein Subdirectory scripts anlegen. Der Befehl roscd erlaubt das schnelle
wechseln in das entsprechende directory des packages
```
    $ roscd pub_sub_tutorial
    $ mkdir scripts
    $ cd scripts
```
6. Jetzt können wir die beiden Python Skripte talker.py und listener.py nach scripts
kopieren. Das kann über die Konsole passieren oder per Copy&Paste. Wenn die Python
Skripte im scripts directory sind, müssen sie noch ausführbar gemacht werden.
```
    $ chmod +x talker.py
    $ chmod +x listener.py
```
7. Noch können wir das package nicht benutzen, da die ausführbaren Python Skripte dem
ROS-System noch mitgeteilt werden müssen. Hier muss die Datei CMakeLists.txt angepasst
werden.
```
catkin_install_python(PROGRAMS scripts/talker.py scripts/listener.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
```
8. Jetzt wird wieder catkin_make benutzt werden, um das package zu bauen.
9. Die Skripte werden mit rosrun gestartet, jedoch darauf achten, dass der roscore
läuft. die Syntax für rosrun ist: rosrun <package_name> <node_name>
Wenn der namespace richtig gesetzt ist - siehe 4. - dann wird der package_name und
der node_name per autocompletion erkannt. Starten wir zuerst den talker.
```
    $ roscore
    $ rosrun pub_sub_tutorial talker.py
```
10. Wenn der talker läuft kann man bereits mit rostopic list und rostopic echo arbeiten.
rostopic list zeigt die momentan gepublishden topics an und mit rostopic echo /topc_name
können sie angezeigt werden.
```
    $ rostopic echo /chatter
```
zeigt das von uns generierte topic an. Wird der listener gestartet, so zeigt er ebenfalls
das topic an.
```
    $ rosrun pub_sub_tutorial listener.py
```
