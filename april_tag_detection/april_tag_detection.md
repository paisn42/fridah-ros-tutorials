1. April_Tags sind 2D-Marker, deren 6D-Pose aus monokularen Bildern extrahiert werden kann.
Das package apriltag_ros kann für ROS Noetic direkt installiert und verwendet werden.
[apriltag](http://wiki.ros.org/apriltag_ros)

```
$ sudo apt install ros-noetic-apriltag-ros
```
Hierfür ist kein extra package zu generieren, sondern es kann ein weiterer launch-file
zum Start des apriltag detectors benutzt werden, der auf die USB Kamera zugreift. Der
Standard launch-file für den apriltag Detektor ist continous_detection.launch, um auf
einen Videostream zuzugreifen.

2. Der launch-file continous_detection.launch muss modifiziert werden, so dass auf
das image topic der USB Kamera zugegriffen werden kann. camera_name muss hierbei auf 
/usb_cam und das image_topic auf image_raw gesetzt werden. Folgende Einträge müssen
demnach im launch-File geändert werden:
```
  <arg name="camera_name" default="/usb_cam" />
  <arg name="image_topic" default="image_raw" />
```  

3. Zusätzlich müssen die benutzten apriltags in zwei Konfigurationsdateien spezifiziert
werden. Hierzu werden YAML Dateien benutzt, die eine einfachere Syntax als XML bzw. JSON
bieten. Ein weiteres directory config sollte angelegt werden, wo die Datei settings.yaml
und tags.yaml nineinkopiert werden.

4. Der settings.yaml file enthält die apriltag Familie, also Angaben dazu, welche Art von
apriltags verwendet werden soll. Hier ist als Standard die tag_family: 'tag36h11' eingetragen.
Das kann so bleiben. Im tags.yaml file müssen aber die zu detektierenden tags gelistet sein,
z.B. so:
```
    standalone_tags:
  [
    {id: 0, size: 0.032, name: tag1},
    {id: 1, size: 0.032},
    {id: 2, size: 0.032},
    {id: 3, size: 0.032}
  ]
```
Hier müssen die ids, die Größen und können die Namen für die Tags angegeben werden.

5. Der Name des launch-files sollte ebenfalls z.B. auf usb_cam_detection.launch geändert
werden, da es ansonsten im namespace zu Überschneidungen kommen kann.

6. Sind alle Änderungen gemacht dann nochmal das package mit catkin_make aktualisieren.
Danach können beide launch Files in separaten Konsolen gestartet werden.

7. Um den Aufwand beim Start zu minimieren kann auch ein launch-File für den Start der USB-Kamera
und die apriltag Detecktion gestartet werden.

8. Zur Visualisierung der detektierten Tags wird rviz eingesetzt. Nach dem Aufruf von rviz kann
zum topic tag_detections_image subscribed werden, um sich ein Bild mit den detektierten tags anzusehen. 
