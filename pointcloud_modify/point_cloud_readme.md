1. Als erste Übung soll eine point_cloud erstellt und gepublished werden. Das kann recht komfortabel
mit numpy erfolgen. Zuerst muss allerdings ein neues package generiert werden, welches
von sensor_msgs rospy und std_msgs abhängt.

2. Das pointcloud_publish.py Skript in das Subdirectory scripts kopieren und ausführbar machen.

3. Bitte im Quellcode darauf achten, das zu einer "frame_id" gepublished wird, wozu es eine
Transformation gibt, z.B. zu "map"

4. Im Quelltext das topic angeben zu dem die point_cloud gepublished werden soll.

5. Dann die CMakeLists.txt ändern und catkin_make ausführen.

6. Mit rosrun kann der Knoten dann gestartet werden.

7. Mit rviz wird die point_cloud anschließend visualisiert.
