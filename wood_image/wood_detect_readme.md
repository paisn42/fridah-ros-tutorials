1. In der Desktop-Standardinstallation von ROS wird OpenCV mit installiert.
Bilder werden ebenfalls über topics verschickt. So erzeugt z.B. das package usb_cam
Bilder unter dem topic /usb_cam/image_raw. Allerdings müssen diese Bilder zur Nutzung
mit OpenCV die "cv_bridge" benutzen - eine Umwandlung von topics in das OpenCV Mat Format.
![cv_bridge]( cvbridge3.png "cv_bridge")
Das folgende Tutorial benutzt das usb_cam package zur Generierung der Bilddaten.
Falls usb_cam nicht installiert ist kann mit
```
$ sudo apt install ros-noetic-usb_cam
```
das package nachträglich installiert werden.

2. Das neue package muss - wie im ersten Tutorial - erzeugt werden. Das 
package soll wood_image_detect heißen und von sensor_msgs, cv_bridge, rospy und std_msgs
abhängig sein.

3. Dann wieder ein scripts subdirectory erzeugen und den File woody_detect.py nach scripts
kopieren und ausführbar machen.

4. Auch hier muss CMakeLists.txt angepasst werden, so dass der Python script ausführbar wird.

5. Schließlich mit catkin_make einen package build machen.

6. Unser neuer woody_detect Knoten braucht ein topic mit Bildern, z.B. von einer USB Kamera.
Es muss also wieder der roscore gestartet werden und der usb_cam node ebenfalls. Da wir bereits
gesehen haben, dass dafür jede Menge Dinge an der Konsole gestartet werden müssen, gibt es dafür
eine komfortable Lösung - launch-files. Der usb_cam Knoten wird daher folgendermaßen gestartet.
```
    $ roslaunch usb_cam usb_cam-test.launch
```
Hier wird die USB-Kamera mit einer Auflösung von "nur" 640x480 gestartet, wie im dargestellten
launch-file gelistet. Diesen launch-file im XML Format kann man aber editieren.
```
<launch>
  <node name="usb_cam" pkg="usb_cam" type="usb_cam_node" output="screen" >
    <param name="video_device" value="/dev/video0" />
    <param name="image_width" value="640" />
    <param name="image_height" value="480" />
    <param name="pixel_format" value="yuyv" />
    <param name="camera_frame_id" value="usb_cam" />
    <param name="io_method" value="mmap"/>
  </node>
  <node name="image_view" pkg="image_view" type="image_view" respawn="false" output="screen">
    <remap from="image" to="/usb_cam/image_raw"/>
    <param name="autosize" value="true" />
  </node>
</launch>
```
Wie man erkennt wir das erste video_device benutzt und die Auflösung gesetzt. Weiterhin wird einzusätzlicher Knoten image_view aufgerufen, um das Bild zu zeigen.

7. Für unsere Anwendung sollte ein angepasster launch-file generiert werden. Daher zuerst ein
directory launch generieren und den launch-file logitech_cam.launch verwenden, allerdings mit
dem richtigen /dev/video statement. Wird der image_view nicht benötigt, kann er aus dem launch
file gelöscht werden, bzw. kann der Aufruf ebenfalls woody_detect.py starten.

8. roslaunch startet ebenfalls den roscore, so dass alle weiteren Knoten einfach mit rosrun
gestartet werden können. Jetzt kann - wenn nicht im launch-file bereits gestartet - auch der
woody_detect Knoten gestartet werden.
```
    $ rosrun wood_image_detect woody_detect.py
```
9. Das Bild der USB Kamera wird in den HSV Farbbereich transferiert und man kann die verschiedenen
Filterwerte beeinflussen. Der Python Skript kann auch mit einem Publisher erweitert werden, der
das gefilterte Bild weitergibt.

