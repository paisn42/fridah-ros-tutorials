# FriDAH ROS Tutorials
Hier sind alle Files verfügbar für das ROS Tutorial im Rahmen des FriDAH Projektes.
Es sind insgesamt vier Tutorials, die verschiedene Aspekte des Einsatzes von ROS
im Projekt verdeutlichen sollen. 

## Getting started

### Erstes Tutorial: Publisher Subscriber
Essentieller Bestandteil von ROS ist die Kommunikation.
Topics werden häufig eingesetzt, um Daten auszutauschen, die
asynchron verwendte werden können, z.B. Sensordaten, Statusdaten usw.
Das erste Tutorial ist ein einfaches Publisher/Subscriber Tutorial, welches direkt
aus den Standard ROS Tutorials generiert wurde.
Die notwendigen Files liegen in /Pub_Sub.

### Zweites Tutorial HSV Filter mit OpenCV
Im FriDAH Projekt stellt die digitale Bildverarbeitung ein Schwerpunkt z.B. zur
Detektion der Holzproben dar. ROS kann zusammen mit OpenCV und entsprechenden
Kameratreibern sehr effektiv zur Analyse von 2D Bilddaten aber auch von 3D Szenarien
eingesetzt werden.  

### Drittes Tutorial: Erkennung von AprilTags
Die Lokalisierung von Objekten in Bildern ist recht komfortabel über Marker möglich.
AprilTags sind weit verbreitet zur Detektion von 3D-Posen in Bilddaten, um so z.B.
relative Greifpositionen von Objekten zu generieren.

### Viertes Tutorial: Darstellung und Bearbeitung von Punktwolken
Moderne 3D Sensorik liefert häufig Punktwolken, die mit ROS ebenfalls verarbeitet und
für die Lokalisierung aber auch Manipulation eingesetzt werden können.

